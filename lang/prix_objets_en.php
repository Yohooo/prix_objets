<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix_objets?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter' => 'Add',
	'ajouter_prix' => 'Add a price',

	// C
	'champ_prix_total_label' => 'Total amount',
	'choisir' => 'Select',
	'choix_devise' => 'Devise',
	'choix_devises' => 'Select a devise:',
	'choix_prix' => 'The products price',

	// D
	'devise_default' => 'Default Devise:',
	'devises_choisis' => 'Selected devises:',

	// E
	'explication_devise_default' => 'Defined by the plugin <a href="/ecrire/?exec=configurer_intl">Bibliothèque d’internationalisation</a>',
	'explication_devises' => 'The currencies to be used for your prices. If nothing is chosen, the default currency will be used. ',

	// F
	'frais_livraison' => 'Shipping costs:',

	// H
	'htva' => 'ex VAT',

	// I
	'info_1_prix' => 'Attached price',
	'info_actions' => 'Actions',
	'info_aucun_prix_objets' => 'No price',
	'info_extensions' => 'Extensions',
	'info_nb_prix' => '@nb@ prices attached',
	'info_prix' => 'Price',
	'infos_generales' => 'General Informations',
	'infos_produits' => 'Information Products',

	// L
	'label_prix_par_objet_mode' => 'Price calculation method:',
	'label_selection_objet_prix' => 'Add a price on the following objects :',
	'legende_prix_par_objet' => 'Price calculation per object',

	// P
	'prix' => 'Price',
	'prix_choisis' => 'Selected Price',
	'prix_ht' => 'Price, ex VAT',
	'prix_ttc' => 'Price,inc VAT',

	// R
	'rubrique_prix' => 'Sections where prices will be diplayeds',

	// T
	'taxes' => 'Taxes',
	'taxes_defaut' => 'Default Taxe',
	'taxes_inclus' => 'Taxe included int the price?',
	'taxes_surcharge' => 'Apply a different rate (in%)',
	'titre_page_configurer_prix_objets_objets' => 'Configure Price Objects',
	'titre_prix_objets' => 'Price',
	'titre_prix_par_objet_mode_global' => 'Global (price of the first coincidence applied globally)',
	'titre_prix_par_objet_mode_prorata' => 'Prorata (average of the prices of each element of a sequence - for example each date of a period)',
	'ttc' => 'inc VAT'
);
