<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

function action_eliminer_prix_dist(){
	include_spip('inc/minipres');
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	$where = array('id_prix_objet='._request('arg'));
	sql_delete('spip_prix_objets',$where);

	// Dans le cas où le plugin déclinaisons est activé, on doit faire 2 fois plus de ménage
	include_spip('inc/plugin');
	if (plugin_est_installe('declinaisons')) {
		sql_delete(
			'spip_prix_objets',
				array (
					"id_prix_objet_source = ". _request('arg'),
					"extension = 'declinaison'"
				)
		);
	}

}
?>